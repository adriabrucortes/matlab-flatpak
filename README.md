# matlab-flatpak

## Roadmap
This project is aimed at creating an installer for the scientific programming language MATLAB®.

To do it, there are 2 possibilites:

Attempt to install MATLAB directly into a flatpak (problematic since it's not legal to redistribute so there would have to be some manouvring in order to create a working MATLAB install) - It would be necessary to find all the dependencies.

Use something like Distrobox to install MATLAB inside an Ubuntu terminal and create a symlink in /app/bin that points inside the Distrobox container (this option might end up being easier but more heavy).

## To-do list:
- [ ] First flatpak - I was able to get it to launch but there are a lot of missing dependencies (shared libraries)
- [ ] Test MATLAB inside a flatpak fo find dependencies or try to use Distrobox inside a flatpak and install MATLAB from scratch